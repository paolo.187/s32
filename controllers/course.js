const Course = require("../models/Course");
const	User = require("../models/User");


// module.exports.allCourses = () => {
// 	return Course.find().then(result=>{
// 		return result
// 	})
// }


module.exports.addCourse = (reqBody) => {

	let newCourse = new Course ({

		name:reqBody.name,
		description:reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course,error)=>{
		if (error) {
			return false
		} else {
			return true
		}
	})
}
//nov3 activity solution if by controller 2nd solution

/*
module.exports.addCourse = async(user,reqBody) => {
	if (user.isAdmin== true){

		let newCourse = new Course ({
		name:reqBody.name,
		description:reqBody.description,
		price: reqBody.price
	})
	}
	return newCourse.save().then((course,error)=>{
		if (error) {
			return false
		} else {
			return true
		}
	})

	else{
	return("not authorized")
	}
*/


//////////////////NOV 4////////////////////////////

//retrieve all courses

module.exports.getAllCourses = () => {

	return Course.find({}).then(result =>{
		return result
	})
};



//retrieve all active courses
module.exports.getAllActive = () => {

	return Course.find({isActive:true}).then(result =>{
		return result
	})
};

//retrieve a specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		// return result or -
		let CourseDetails = {
			name: result.name,
			price: result.price
		}
		return CourseDetails
		
	})
}

//update course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//findByIdAndUpdate (document,updatesToBeAplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,err)=>{
		if (err){
			return false
		}else {
			return true
		}
	})
}


//AQctivity // archive a course

module.exports.archiveCourse = (reqParams, reqBody) => {
	let courseForArchive = {
		isActive:reqBody.isActive
	}
	//findByIdAndUpdate (document,updatesToBeAplied)
	return Course.findByIdAndUpdate(reqParams.courseId, courseForArchive).then((course,err)=>{
		if (err){
			return false
		}else {
			return true
		}
	})
}