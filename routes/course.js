const express = require("express");
const router = express.Router();
const courseController = require("../controllers/course");
const	auth = require ("../auth");





// Activity Nov3 add course with jwt




router.post("/",auth.verify, (req,res)=> {

		const courseData = auth.decode(req.headers.authorization)

		if (courseData.isAdmin == true){
			courseController.addCourse(req.body).then(resultFromController =>
			res.send(resultFromController))
		}else{
			console.log("not authorized")
			res.send("not authorized")
		}
	 
})


//Activity 2nd solution

/*
router.post("/",auth.verify, (req,res)=> {

		const courseData = auth.decode(req.headers.authorization)

			courseController.addCourse(req.body).then(resultFromController =>
			res.send(resultFromController))
		}
	 
*/


//////////////////NOV 4////////////////////////////

//retrieve all courses

router.get("/all", (req,res)=>{
	courseController.getAllCourses().then(resultFromController =>
		res.send(resultFromController))
})


//retrieve all active courses
router.get("/", (req,res)=>{
	courseController.getAllActive().then(resultFromController =>
		res.send(resultFromController))
})



//retrieve specific courses using
router.get("/:courseId", (req,res)=> {
	
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController =>
		res.send(resultFromController))
})


//update a course
router.put("/:courseId", auth.verify, (req,res) =>{
	courseController.updateCourse(req.params, req.body).then(resultFromController =>
		res.send(resultFromController))
})



//Activity  // archive a course



router.put("/:courseId/archive", auth.verify, (req,res) =>{
	const courseData = auth.decode(req.headers.authorization)


		if (courseData.isAdmin == true){
			courseController.archiveCourse(req.params, req.body).then(resultFromController =>
			res.send(resultFromController))
		}else{
			console.log("not authorized")
			res.send("not authorized")
		}

	
})




module.exports = router;