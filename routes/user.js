const	express = require("express");
const	router = express.Router();
const	userController = require("../controllers/user")
const	auth = require ("../auth");

router.post("/checkEmail", (req,res)=> {
	userController.checkEmailExists(req.body).then(resultFromController => 
		res.send(resultFromController))
})


router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(resultFromController =>
		res.send(resultFromController))
})

router.get("/allUsers", (req,res)=> {
	userController.allUsers().then(resultFromController=>
		res.send(resultFromController))
})

router.post("/login", (req,res)=> {
	userController.loginUser(req.body).then(resultFromController =>
		res.send(resultFromController));
})



////Activity: retrieve user details by id with jwt

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId: req.body.id}).then(resultFromController => 
		res.send(resultFromController))
});

//For enrolling a user

// router.post("/enroll",(req,res) => {

// 	let data = {
// 		userId:req.body.userId,
// 		courseId:req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController =>
// 		res.send(resultFromController))

// });


//Activity

router.post("/enroll",auth.verify,(req,res) => {
		const userData = auth.decode(req.headers.authorization)

		if (userData.isAdmin == true){
			res.send("Admin not authorized to enroll")
		}else {

			let data = {
				//so logged in only user can enroll himself
				userId:userData.id,
				courseId:req.body.courseId
			}

			userController.enroll(data).then(resultFromController =>
				res.send(resultFromController))


		}

});



module.exports = router;